# Django example

Base django application with docker

## Requirements
- docker
- docker-compose

## Installation
1. clone this repository

    `git clone https://gitlab.com/ragutierrez/django.git`

2. open `example` folder

    `cd django/example`

3. execute docker-compose to build image and containers

    `docker-compose up --no-start`

4. migrate models 

    `docker-compose run --rm django python manage.py migrate`

5. create a superuser

    `docker-compose run --rm django python manage.py createsuperuser`

6. execute docker-compose to start containers

    `docker-compose up -d`

7. that's it happy codding!!!

PS: if you have questions, please ask google, or ask me then, but google knows more than me!