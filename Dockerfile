FROM python:3.8-slim

# RUN apt-get update
# RUN apt -y upgrade
# RUN apt-get -y install --no-install-recommends

RUN useradd -U -u 1000 django

ENV PYTHONUNBUFFERED 1
RUN mkdir /mnt/code
WORKDIR /mnt/code
COPY example/requirements.txt /mnt/code/
RUN pip3 install --no-cache-dir -r requirements.txt

RUN chown -R django:django /mnt/code

USER django

